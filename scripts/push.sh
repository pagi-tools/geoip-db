#!/bin/bash

git add .
git commit -m "[Robot] Upload geoip db"
git pull
ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
git push git@gitlab.com:pagi-tools/geoip-db.git HEAD:main
