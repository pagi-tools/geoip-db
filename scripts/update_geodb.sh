#!/bin/sh

set -e

# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}

trap 'kill ${!}; term_handler' SIGTERM

pid=0
conf_file=/etc/GeoIP.conf
database_dir=${PWD}/data
flags=
frequency=$((GEOIPUPDATE_FREQUENCY * 60 * 60))

if ! [ -z "$GEOIPUPDATE_CONF_FILE" ]; then
  conf_file=$GEOIPUPDATE_CONF_FILE
fi

if ! [ -z "$GEOIPUPDATE_DB_DIR" ]; then
  database_dir=$GEOIPUPDATE_DB_DIR
fi

if [ -z "$GEOIPUPDATE_ACCOUNT_ID" ] || [ -z  "$GEOIPUPDATE_LICENSE_KEY" ] || [ -z "$GEOIPUPDATE_EDITION_IDS" ]; then
    echo "ERROR: You must set the environment variables GEOIPUPDATE_ACCOUNT_ID, GEOIPUPDATE_LICENSE_KEY, and GEOIPUPDATE_EDITION_IDS!"
    exit 1
fi

# Create configuration file
echo "# STATE: Creating configuration file at $conf_file"
cat <<EOF > "$conf_file"
AccountID $GEOIPUPDATE_ACCOUNT_ID
LicenseKey $GEOIPUPDATE_LICENSE_KEY
EditionIDs $GEOIPUPDATE_EDITION_IDS
EOF

if [ ! -z "$GEOIPUPDATE_HOST" ]; then
    echo "Host $GEOIPUPDATE_HOST" >> "$conf_file"
fi

if [ ! -z "$GEOIPUPDATE_PROXY" ]; then
    echo "Proxy $GEOIPUPDATE_PROXY" >> "$conf_file"
fi

if [ ! -z "$GEOIPUPDATE_PROXY_USER_PASSWORD" ]; then
    echo "ProxyUserPassword $GEOIPUPDATE_PROXY_USER_PASSWORD" >> "$conf_file"
fi

if [ ! -z "$GEOIPUPDATE_PRESERVE_FILE_TIMES" ]; then
    echo "PreserveFileTimes $GEOIPUPDATE_PRESERVE_FILE_TIMES" >> "$conf_file"
fi

if [ "$GEOIPUPDATE_VERBOSE" ]; then
    flags="-v"
fi

#########
# Slack #
#########
# URL="$SLACK_WEBHOOK"
# CHANNEL="$SLACK_CHANNEL"
# PAYLOAD='{
#   "channel": "#lee-test",
#   "username": "worker",
#   "text": "",
#   "icon_emoji": ":doge:"
# }'

# CHECK jq installed
exit_with_err() {
  echo >&2 "I require jq but it's not installed. Aborting."; exit 1;
}
command -v jq >/dev/null 2>&1 || { exit_with_err; }
type jq >/dev/null 2>&1       || { exit_with_err; }
hash jq 2>/dev/null           || { exit_with_err; }

while true; do
    echo "# STATE: Running geoipupdate"
    /usr/local/bin/geoipupdate -d "$database_dir" -f "$conf_file" $flags
	# Send slack message
	# if [ $? -eq 0 ]; then
    #     body=$(echo $PAYLOAD | jq -c --arg msg "Successfully updated GeoLite2 DB" '.text = $msg' | jq -c --arg channel "${SLACK_CHANNEL}" '.channel = $channel')
	# else
    #     body=$(echo $PAYLOAD | jq -c --arg msg "Failed to update GeoLite2 DB" '.text = $msg' | jq -c --arg channel "${SLACK_CHANNEL}" '.channel = $channel')
	# fi
    # curl -X POST -H "Content-Type: application/json" -d "$body" $URL

    if [ "$frequency" -eq 0 ]; then
        break
    fi

    echo "# STATE: Sleeping for $GEOIPUPDATE_FREQUENCY hours"
    sleep "$frequency" &
    pid=$!
    wait $!
done
