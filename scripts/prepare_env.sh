#!/bin/bash

# init key
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo "$ROBOT_PK" > ~/.ssh/id_rsa
chmod 400 ~/.ssh/*

# init git
git checkout main
git remote set-url --push origin "git@gitlab.com:pagi-tools/geoip-db.git"
git checkout main
git pull
git config --global user.email "robot@pagi.com"
git config --global user.name "robot"
